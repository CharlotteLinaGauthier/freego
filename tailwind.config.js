module.exports = {
  purge: ["./public/**/*.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        pink: {
          main: "#CD5D7D",
          light: "#F1F9F9",
        },
        black: {
          main: "#404040",
        },
        blue: {
          main: "#A7C5EB",
          light: "F1F9F9",
        },
        indigo: {
          main: "#7879F1",
        },
      },
      boxShadow: {
        pinkbottom: "0 2px 4px 0px #CD5D7D88",
        darktop: "0 2px 4px 0px #707070",
        pinktop: "0 -2px 4px 0px #CD5D7D88",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
