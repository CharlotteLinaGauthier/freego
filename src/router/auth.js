import Freego from "@/pages/freego/Freego";
import Cart from "@/pages/Cart";
import main from "@/pages/main";
import Recipes from "@/pages/recipes/Recipes";
import RecipesRecommendation from "@/pages/recipes/RecipesRecommendation";
import RecipesItem from "@/pages/recipes/RecipesItem";
import Friends from "@/pages/Friends";
import Logout from "@/pages/auth/Logout";
import FreegoAddIngredient from "@/pages/freego/AddIngredient";
import FreegoAddIngredientItem from "@/pages/freego/AddIngredientItem";
import UserAddAllergies from "@/pages/user/AddAllergies";
import User from "@/pages/user/User";

export default [
  {
    path: "/",
    name: "main",
    component: main,
    meta: {
      guard: ["auth"],
    },
  },
  {
    path: "/freego",
    name: "freego",
    component: Freego,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/freego/add-ingredient",
    name: "freegoAddIngredient",
    component: FreegoAddIngredient,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/freego/add-ingredient/:id",
    name: "freegoAddIngredientItem",
    component: FreegoAddIngredientItem,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/cart",
    name: "cart",
    component: Cart,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/friends",
    name: "friends",
    component: Friends,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/recipes",
    name: "recipes",
    component: Recipes,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/recipes-recommendation",
    name: "recipesRecommendation",
    component: RecipesRecommendation,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/recipes/:id",
    name: "recipes-item",
    component: RecipesItem,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/logout",
    name: "logout",
    component: Logout,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/user",
    name: "user",
    component: User,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  {
    path: "/user/add-allergies",
    name: "userAddAllergies",
    component: UserAddAllergies,
    meta: {
      guard: ["auth"],
      layout: "default",
    },
  },
  
];
