import Vue from "vue";
import Vuex from "vuex";

import user from "@/store/modules/user";
import freego from "@/store/modules/freego";
import layout from "@/store/modules/layout";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    user,
    freego,
    layout,
  },
  strict: true,
});

export default store;
