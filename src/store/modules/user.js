const state = {
  user: null,
  freegos: [],
};

const getters = {
  user(state) {
    return state.user;
  },
  freegos(state) {
    return state.freegos;
  }
};

const actions = {
  setUser({ commit }, user) {
    console.log('user', user);
    commit({
      type: "SET_USER",
      user,
    });
  },
  setFreegos({ commit }, freegos) {
    console.log('freegos', freegos);
    commit({
      type: "SET_FREEGOS",
      freegos
    });
  }
};

const mutations = {
  SET_USER(state, payload) {
    state.user = payload.user;
  },
  SET_FREEGOS(state, payload) {
    state.freegos = payload.freegos;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
