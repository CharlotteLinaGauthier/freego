import Vue from "vue";
import VueRouter from "vue-router";
// import store from "@/store";
// import Axios from "axios";

import AuthRoute from "@/router/auth";
import Public from "@/router/public";

Vue.use(VueRouter);

const routes = [...Public, ...AuthRoute];

const router = new VueRouter({
  mode: "history",
  linkExactActiveClass: "text-pink-main bg-white",
  routes,
});

router.beforeEach(async (routeTo, routeFrom, next) => {
  // let guards = routeTo.meta.guard;
  // // console.log(routeTo, routeFrom);
  // if (!guards || !guards.length) {
  //   // Public Route
  //   next();
  //   return;
  // }

  // let user = store.getters["user/user"];
  // if (!user) {
  //   await axios.get("/user/me").then((res) => {
  //     console.log(res);
  //   }).catch(e => {
  //     console.error(e);
  //     // store.dispatch("user/user", { id: 1, username: "hugo", email: "hugo.venancio@test.fr" });
  //     // user = store.getters["user/user"];
  //   })
  // }

  // if (guards.includes('auth') && user == null) {
  //   console.log('redirecting to login');
  //   console.log(user);
  //   next({ name: 'login' });
  //   return;
  // }

  // if (guards.includes('noauth') || routeTo.name == 'main' && user != null) {
  //   console.log('redirecting to freego');
  //   console.log(user);
  //   next({ name: 'freego' });
  //   return;
  // }

  next();
});

export default router;
