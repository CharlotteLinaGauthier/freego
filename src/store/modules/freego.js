const state = {
    freego: {
        ingredients: [
            {
                name: "Apple",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 2,
                img: "https://cdn.picpng.com/apple/apple-view-25231.png",
            },
            {
                name: "Bread",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 3,
                img: "https://esquilo.io/png/thumb/zZEiMuRyK8GIAp6-French-Baguette-Bread-PNG.png",
            },
            {
                name: "Bread",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 3,
                img: "https://esquilo.io/png/thumb/zZEiMuRyK8GIAp6-French-Baguette-Bread-PNG.png",
            },
            {
                name: "Apple",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 2,
                img: "https://cdn.picpng.com/apple/apple-view-25231.png",
            },
            {
                name: "Apple",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 2,
                img: "https://cdn.picpng.com/apple/apple-view-25231.png",
            },
            {
                name: "Bread",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 3,
                img: "https://esquilo.io/png/thumb/zZEiMuRyK8GIAp6-French-Baguette-Bread-PNG.png",
            },
            {
                name: "Bread",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 3,
                img: "https://esquilo.io/png/thumb/zZEiMuRyK8GIAp6-French-Baguette-Bread-PNG.png",
            },
            {
                name: "Apple",
                quantity: 1,
                dlc: new Date(),
                dlcExpiresIn: 2,
                img: "https://cdn.picpng.com/apple/apple-view-25231.png",
            },
        ],
    },
};

const getters = {

    freego(state) {
        return state.freego;
    },
    ingredients(state) {
        return state.freego.ingredients;
        // return state.freego.ingredients.filter(item => item.quantity > 0);
    },
};

const actions = {
    freego({ commit }, freego) {
        commit({
            type: "SET_FREEGO",
            freego,
        });
    },
};

const mutations = {
    SET_FREEGO(state, payload) {
        state.freego = { ...state, freego: payload.freego };
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
};
