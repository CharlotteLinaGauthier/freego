import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "./assets/tailwind.css";
import Default from "@/layouts/default/index";
import Auth from "@/layouts/auth/index";
import Axios from "axios";
import VueCookie from "vue-cookie";
import Toast from "vue-toastification";

Vue.use(VueCookie);

Vue.config.productionTip = false;

Vue.component("default-layout", Default);
Vue.component("auth-layout", Auth);

Vue.prototype.$axios = Axios.create({
  baseURL: "http://localhost:5001",
});

Vue.use(Toast, {
  timeout: 2500,
});

Vue.prototype.$axios.interceptors.request.use(
  (config) => {
    let accessToken = VueCookie.get("_token");
    if (accessToken) {
      config.headers = Object.assign(
        {
          Authorization: `Bearer ${accessToken}`,
        },
        config.headers
      );
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
