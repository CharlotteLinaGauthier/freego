export default {
  computed: {
    layout() {
      const default_layout = "default";
      return (this.$route.meta.layout || default_layout) + "-layout";
    },
  },
};
