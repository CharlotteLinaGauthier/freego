import Login from "@/pages/auth/Login";
import Register from "@/pages/auth/Register";

export default [
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      guard: ["noauth"],
      layout: "auth",
    },
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      guard: ["noauth"],
      layout: "auth",
    },
  },
];
