# Freego-Front

Partie Front-end de l'application

## Description

Application principalement la gestion de son frigo. Avec la recherche de recette avec les ingrédients disponible dans le frigo, la réalisation de liste de course ou encore des listes de courses partagés.  
Cette application est réalisé dans le cadre d'un projet scolaire.

## Roadmap

Sprint 0 : Architecture, écriture du cachier des charges, maquettes de l'application, choix des technologies  
Sprint 1 : Initialisation Projet / Mise en place du Frigo  
Sprint 2 :  
Sprint 3 :

## Authors

Rollet Beatrice, Venancio Hugo, Bordand Mathieu, Penot Dorian, Gauthier Charlotte

## Project status

Initialisation
