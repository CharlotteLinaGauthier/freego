const state = {
  showFooter: true,
};

const getters = {
  showFooter(state) {
    return state.showFooter;
  },
};

const actions = {
  showFooter({ commit }, showFooter) {
    commit({
      type: "SET_SHOW_FOOTER",
      showFooter,
    });
  },
};

const mutations = {
  SET_SHOW_FOOTER(state, payload) {
    state.showFooter = payload.showFooter;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
